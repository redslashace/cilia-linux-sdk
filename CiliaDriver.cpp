#include "CiliaDriver.hpp"

int main(void)
{
	CiliaServer CiliaServerInstance;
	CiliaSerial CiliaSerialInstance;
	CiliaDriver CiliaDriverInstance;
	std::string receiveBuffer = " ";
	std::string messageToSendString("C");
	int openPortStatus;
	std::vector<CiliaSerial> serialVector;

	DIR * directoryInstance;
	dirent * directoryEntryInstance;
	directoryInstance = opendir("/sys/class/tty");
	std::string testdirstring(" ");	
	while((directoryEntryInstance = readdir(directoryInstance)) != NULL)
	{
		testdirstring = directoryEntryInstance->d_name;
		if(testdirstring.find("USB") != std::string::npos)
		{
			CiliaSerialInstance.SetComPortName(std::string("/dev/" + testdirstring));
			openPortStatus = CiliaSerialInstance.OpenComPort();
			if(openPortStatus != -1)
			{
				CiliaSerialInstance.SendMessage(messageToSendString);
				receiveBuffer = "";
				CiliaSerialInstance.ReceiveMessage((std::string *) &receiveBuffer);
			}
		}
		if((testdirstring.find("USB")  != std::string::npos)  && ( receiveBuffer.find("CILIA") != std::string::npos ))
		{
			serialVector.push_back(CiliaSerialInstance);
			std::cout<< "Cilia Found On Port: $$$" << testdirstring <<"\n";
		}
	}

	closedir(directoryInstance);

	if(serialVector.size() == 0)
	{
		std::cout << "Cilia Not Found\n";
		return -1;
	}

	for(CiliaSerial cS : serialVector)
	{
		cS.SendMessage("N1000255255");
		cS.SendMessage("N2000255255");
		cS.SendMessage("N3000255255");
		cS.SendMessage("N4000255255");
		cS.SendMessage("N5000255255");
		cS.SendMessage("N6000255255");
		cS.SendMessage("F1000");
		cS.SendMessage("F2000");
		cS.SendMessage("F3000");
		cS.SendMessage("F4000");
		cS.SendMessage("F5000");
		cS.SendMessage("F6000");
	}
	if(CiliaServerInstance.OpenServer() != 0)
			std::cout << "Open Server Failed\n";
	while(true)
	{
		while(true)
		{
			CiliaServerInstance.ServerReceiveMessage((std::string *) &receiveBuffer);
			std::cout << "$" << receiveBuffer << "$\n";
			for(char c : receiveBuffer)
			{
				printf("0x%x",c);
			}
			std::cout<< "\ntest\n";

			if(receiveBuffer.empty() || (receiveBuffer.find("CLOSE") != std::string::npos))
			{
				std::cout << "close\n";
				break;
			}

			for(CiliaSerial cS : serialVector)
			{
				cS.SendMessage(receiveBuffer);
			}
		}
		CiliaServerInstance.CloseServer();
		if(CiliaServerInstance.ReOpenServer() != 0)
			std::cout << "Re Open Server Failed\n";
	}
	getchar();

	std::cout << "Closing Cilia Driver\n";
	for(CiliaSerial cS : serialVector)
	{
		int closePortStatus = cS.CloseComPort();
	}
	return 0;
}

CiliaDriver::CiliaDriver()
{	
}



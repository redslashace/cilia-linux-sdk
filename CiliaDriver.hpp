/*
*	Cilia Driver for Linux
*	@verstion 0.0
*	@author Peter Sassaman
*	@date 09/29/18
*/

#ifndef CiliaDriverHeader
#define CiliaDriverHeader

//Include Files
#include "CiliaSharedDefenitions.hpp"
#include <stdio.h>
#include <stdlib.h>
#include "CiliaSerial.hpp"
#include "CiliaServer.hpp"
#include "dirent.h"
#include <vector>
/*
*	Cilia Driver Class
*/
class CiliaDriver
{
	private:

	public:
	/*Default Constructor*/
	CiliaDriver();
};

#endif

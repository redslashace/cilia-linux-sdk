/*
* Header file for common defines to be shared between multiple classes.
* @Author Peter Sassaman
* @Date Last Upated 1/6/19
*/

#ifndef CILIASHAREDDEFENITIONS_H
#define CILIASHAREDDEFENITIONS_H

/*
	Define for largest buffer used in communicating over serial tcp/ip and possibly other methods in the future.
*/

const static int INPUTBUFFERMAX = 1024;

#endif

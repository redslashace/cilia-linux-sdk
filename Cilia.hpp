#ifndef CILIA_H
#define CILIA_H

#include "string.h"
#include <string>
#include <fstream>

enum class SurroundPosition { Front, FrontLeft, SideLeft, BackLeft, Back, BackRight, SideRight, FrontRight};

class Cilia
{
	private:
	std::string smells[6];
	SurroundPosition position;
	int comPort;
	public:
	/*
		Default Constructor 
		- set smells to some defualt
		- set position to front
	*/
	Cilia();
	/*
	*	COM Port Constructor
	*	- see if file exists. if it does read in the smells and position.
	*	- if not call the default constuctor and then set the position.
	*/
	Cilia(std::string comPortName);
	/*
	*	sets all the smells and updates the file
	*/
	//SetSmells(std::string smells[6]);
	/*
	*	set an individual smell and update the file
	*/
	//SetSmell(std::string smell, int smellNumber);
	/*
	*	set position of the unit and update the file
	*/
	//SetPosition(SurroundPosition position);
	/*
	*	get smells return array of smells
	*/

	/*
	* get specific smell
	*/
	/*
	*	get position
	*/
	~Cilia();
};

#endif

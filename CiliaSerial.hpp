#ifndef CILIASERIAL_H
#define CILIASERIAL_H

/*Include Libraries*/
#include "CiliaSharedDefenitions.hpp"
#include <unistd.h>
#include <termios.h>
#include <errno.h>
#include <fcntl.h>
#include "iostream"
#include "string.h"
#include <string>
/*
*  Class for talking with Cilia Devices over serial port
*  @author Peter Sassaman
*  @date last upated 1/6/19
*/
/*
*	Refferences
*	https://stackoverflow.com/questions/18108932/linux-c-serial-port-reading-writing
*/
class CiliaSerial
{
	private:
	/*Private variables*/
	std::string COMPortName;	
	termios termiosInstance;
	termios termiosCopy;
	int COMX;
	public:
	/**
	* Default Constructor
	*/
	CiliaSerial();
	/**
	* Set the name of the com port
	* @param name to be set
	*/
	void SetComPortName(std::string COMPortNameString);
	/**
	* Returns the name of the com port as a string
	* @return name of com port
	*/
	std::string GetComPortName();
	/**
	* Opens COM Port
	* @return -1 if failure or number of the com port
	*/
	int OpenComPort();
	/**
	* Closes COM Port
	* @return -1 if failure or 0 if successful
	*/
	int CloseComPort();
	/**
	* Sends a string message of the serial port to the Cilia
	* @param stringToSend to the Cilia
	* @return 0
	* Need to check for error  states???
	*/
	int SendMessage(std::string stringToSend);
	/**
	* Receives a string message from the Cilia over the serial port
	* @param pointer to the string that we will be receiving over the serial port
	* @return 0
	* Need to check for error states???
	*/
	int ReceiveMessage(std::string * stringToReceive);
};
#endif
